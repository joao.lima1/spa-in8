import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { In8SpaComponent } from '../Components'

const In8SpaRoutes: Routes = [
    {
        path: 'spa',
        component: In8SpaComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(In8SpaRoutes)],
  exports: [RouterModule]
})
export class In8SpaRoutingModule { }
