import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxMaskModule } from 'ngx-mask';
import { AngularMaterialModule } from '../../Angular-Material';
import {
  AlertComponent, CadastroEstagiarioComponent, In8SpaComponent,
  MainFooterComponent,
  MainFooterMobileComponent, MainImgDesktopComponent,
  MainImgMobileComponent, MainImgTabletComponent,
  ProgressSpinnerDialogComponent,
  TabelaEstagiarioDesktopComponent,
  TabelaEstagiarioMobileComponent,
  TabelaEstagiarioTabletComponent
} from '../Components';
import { AuthInterceptor, LoadingInterceptor } from '../Interceptors';
import { EstagiarioService, FeedbackService, PlataformService, ScrollService } from '../Services';
import { In8SpaRoutingModule } from './in8-spa.routing.module';




@NgModule({
  declarations: [
    In8SpaComponent,
    MainImgDesktopComponent,
    MainImgTabletComponent,
    MainImgMobileComponent,
    CadastroEstagiarioComponent,
    TabelaEstagiarioDesktopComponent,
    TabelaEstagiarioTabletComponent,
    TabelaEstagiarioMobileComponent,
    MainFooterComponent,
    MainFooterMobileComponent,
    AlertComponent,
    ProgressSpinnerDialogComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    In8SpaRoutingModule,
    AngularMaterialModule,
    LayoutModule,
    NgxMaskModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    PlataformService,
    ScrollService,
    EstagiarioService,
    FeedbackService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },
  ],
  entryComponents: [AlertComponent, ProgressSpinnerDialogComponent]
})
export class In8SpaModule { }
