export class Plataform{
    constructor(
        public readonly Desktop:string = 'Desktop',
        public readonly Tablet:string = 'Tablet',
        public readonly Mobile:string = 'Mobile'
    ){}
}