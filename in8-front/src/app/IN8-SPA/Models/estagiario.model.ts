export class Estagiario{
    constructor(
        public nome?: string,
        public email?: string,
        public nascimento?: string,
        public telefone?: string
    ){}
}