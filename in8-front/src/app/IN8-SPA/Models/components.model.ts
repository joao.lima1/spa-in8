export class Components{
    constructor(
        public readonly Main:string = 'Main',
        public readonly Cadastro:string = 'Cadastro',
        public readonly Table:string = 'Table',
        public readonly Footer:string = 'Footer'
    ){ }
}