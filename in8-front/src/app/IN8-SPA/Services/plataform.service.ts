import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Plataform } from '../Models';


@Injectable({
  providedIn: 'root'
})

export class PlataformService {

  plataform: Plataform = new Plataform();
  currentPlataform: BehaviorSubject<string> = new BehaviorSubject<string>(null)
  breakpoints: any[] = [Breakpoints.Handset, Breakpoints.Tablet, Breakpoints.Web]

  constructor(public breakpointObserver: BreakpointObserver) {

    this.breakpointObserver.observe(this.breakpoints).subscribe((state: BreakpointState) => {
      if (state.matches) {
        Object.keys(state.breakpoints).forEach(breakpoint => {
          if (state.breakpoints[breakpoint]) {
            this.breakpoints.forEach(targetBreakpoint => {
              const BreakpointsValues = targetBreakpoint.split(', ');
              BreakpointsValues.forEach(value => {
                if (value === breakpoint) {
                  if (targetBreakpoint === Breakpoints.Handset) {
                    this.setPlataform(this.plataform.Mobile);
                  }
                  else if (targetBreakpoint === Breakpoints.Tablet) {
                    this.setPlataform(this.plataform.Tablet);
                  }
                  else if (targetBreakpoint === Breakpoints.Web) {
                    this.setPlataform(this.plataform.Desktop);
                  }
                }
              });
            })
          }
        })
      }
    })

  }

  setPlataform(newPlataform: string) {
    this.currentPlataform.next(newPlataform);
  }

  getPlataform() {
    return this.currentPlataform.asObservable();
  }

}
