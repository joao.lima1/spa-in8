import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Estagiario } from '../Models';
import { FeedbackService } from './feedback.service';


const {
  Back_URL,
} = environment;

@Injectable({
  providedIn: 'root'
})
export class EstagiarioService {

  estagiarios: BehaviorSubject<Estagiario[]> = new BehaviorSubject<Estagiario[]>([])
  currentArrayEstagiarios: Estagiario[] = []

  constructor(
    private http: HttpClient,
    private feedbackService: FeedbackService
  ) {
    this.getEstagiarios().subscribe(arrayEstagiarios => {
      this.currentArrayEstagiarios = arrayEstagiarios;
    })
  }

  setEstagiarios(newArrayEstagiarios: Estagiario[]) {
    this.estagiarios.next(newArrayEstagiarios);
  }

  getEstagiarios() {
    return this.estagiarios.asObservable();
  }

  salvarEstagiario(newEstagiario: Estagiario) {
    return this, this.http.post(`${Back_URL}/estagiarios`, newEstagiario).subscribe(res => {
      this.currentArrayEstagiarios.push(res);
      this.setEstagiarios(this.currentArrayEstagiarios);
      this.feedbackService.showAlert('Estagiario salvo com sucesso', 'success', 6000)
    },
      err => {
        this.feedbackService.showAlert(err['error']['error']['message'], 'danger', 6000)
      })
  }

  recuperarListaEstagiarios() {
    return this, this.http.get(`${Back_URL}/estagiarios`).subscribe((res: Estagiario[]) => {
      if (res && res.length) {
        this.setEstagiarios(res)
        this.feedbackService.showAlert('Lista de estagiarios recuperada com sucesso', 'success', 6000)
      }
    },
      err => {
        this.feedbackService.showAlert(err['error']['error']['message'], 'danger', 6000)
      })
  }
}
