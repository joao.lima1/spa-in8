import { Injectable } from '@angular/core';
import { Components } from '../Models';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {


  components: Components = new Components();
  componentsRendered: any[] = [
    {
      name: this.components.Main,
      yPosition: 0
    },
    {
      name: this.components.Cadastro,
      yPosition: 0
    },
    {
      name: this.components.Table,
      yPosition: 0
    },
    {
      name: this.components.Footer,
      yPosition: 0
    },
  ]

  constructor() { }

  setYPosition(targetComponent: string, yPosition: number) {
    this.componentsRendered.forEach(component => {
      if (targetComponent === component['name']) {
        component['yPosition'] = yPosition;
      }
    })
  }

  scrollTo(targetComponent: string) {
    setTimeout(() => {
      this.componentsRendered.forEach(component => {
        if (targetComponent === component['name']) {
          window.scrollTo(0, component['yPosition']);
        }
      })
    });
  }
}
