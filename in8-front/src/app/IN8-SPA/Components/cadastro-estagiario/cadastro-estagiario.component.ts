import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Components, Estagiario } from '../../Models';
import { EstagiarioService, ScrollService } from '../../Services';
import { mascaraCelular, mascaraNascimento, mascaraTelefone } from '../../utils';

@Component({
  selector: 'app-cadastro-estagiario',
  templateUrl: './cadastro-estagiario.component.html',
  styleUrls: ['./cadastro-estagiario.component.scss']
})
export class CadastroEstagiarioComponent implements OnInit, AfterViewInit {

  components: Components = new Components();
  mascaraTelefone = mascaraTelefone;
  mascaraNascimento = mascaraNascimento;

  @ViewChild('mainDiv', { static: false }) mainDiv: ElementRef;

  formCadastro = this.formBuilder.group({
    nome: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    nascimento: [null, [Validators.required, Validators.minLength(8)]],
    telefone: [null, [Validators.required, Validators.minLength(10)]]
  })

  constructor(
    private scrollService: ScrollService,
    private formBuilder: FormBuilder,
    private estagiarioService: EstagiarioService
  ) { }

  ngOnInit() {
    this.setMaskTelefone();
    this.estagiarioService.recuperarListaEstagiarios();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.scrollService.setYPosition(this.components.Cadastro, this.mainDiv['nativeElement']['offsetTop'])
    }, 1000);
  }

  setMaskTelefone() {
    this.formCadastro.controls.telefone.valueChanges.subscribe(val => {
      if (val && val.length <= 10) {
        this.mascaraTelefone = mascaraTelefone;
      }
      else {
        this.mascaraTelefone = mascaraCelular;
      }
    },
    );
  }

  criarEstagiario(): Estagiario {
    const NewEstagiario = new Estagiario();
    const formValues = this.formCadastro.getRawValue();
    Object.keys(formValues).forEach(key => {
      NewEstagiario[key] = formValues[key];
    })
    return NewEstagiario
  }

  clearForm() {
    const formValues = this.formCadastro.getRawValue();
    this.formCadastro.reset();
    Object.keys(formValues).forEach(key => {
      this.formCadastro.controls[key].setErrors(null);
    })
    this.formCadastro.updateValueAndValidity();
  }

  cadastrarEstagiario() {
    if (this.formCadastro.valid) {
      this.estagiarioService.salvarEstagiario(this.criarEstagiario());
      this.clearForm();
    }
  }
}
