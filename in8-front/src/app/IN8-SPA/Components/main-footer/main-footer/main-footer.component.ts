import { Component, ElementRef, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { PlataformService } from '../../../Services';
import { Plataform, Components } from '../../../Models';
import { ScrollService } from '../../../Services';

@Component({
  selector: 'app-main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.scss']
})
export class MainFooterComponent implements OnInit, AfterViewInit {

  plataform:Plataform = new Plataform()
  currentPlataform:string;
  components:Components = new Components();

  @ViewChild('mainDiv', { static: false }) mainDiv: ElementRef;

  constructor(
    private plataformService:PlataformService,
    private scrollService: ScrollService
  ) { }

  ngOnInit() {
    this.plataformService.getPlataform().subscribe(plataform =>{
      this.currentPlataform = plataform;
    })
  }

  ngAfterViewInit(){
    setTimeout(() => {
      this.scrollService.setYPosition(this.components.Footer, this.mainDiv['nativeElement']['offsetTop'])
    },1000);
  }

}
