import { Component, ElementRef, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Components } from '../../../Models';
import { ScrollService } from '../../../Services';

@Component({
  selector: 'app-main-footer-mobile',
  templateUrl: './main-footer-mobile.component.html',
  styleUrls: ['./main-footer-mobile.component.scss']
})
export class MainFooterMobileComponent implements OnInit, AfterViewInit {

  components:Components = new Components();

  @ViewChild('mainDiv', { static: false }) mainDiv: ElementRef;

  constructor(private scrollService: ScrollService) { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    setTimeout(() => {
      this.scrollService.setYPosition(this.components.Footer, this.mainDiv['nativeElement']['offsetTop'])
    },1000);
  }

}
