import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainFooterMobileComponent } from './main-footer-mobile.component';

describe('MainFooterMobileComponent', () => {
  let component: MainFooterMobileComponent;
  let fixture: ComponentFixture<MainFooterMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainFooterMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainFooterMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
