import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { In8SpaComponent } from './in8-spa.component';

describe('In8SpaComponent', () => {
  let component: In8SpaComponent;
  let fixture: ComponentFixture<In8SpaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ In8SpaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(In8SpaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
