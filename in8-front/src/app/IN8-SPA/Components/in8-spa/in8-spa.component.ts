import { Component, OnInit } from '@angular/core';
import { PlataformService } from '../../Services';
import { Plataform } from '../../Models'

@Component({
  selector: 'app-in8-spa',
  templateUrl: './in8-spa.component.html',
  styleUrls: ['./in8-spa.component.scss']
})
export class In8SpaComponent implements OnInit {

  plataform:Plataform = new Plataform()
  currentPlataform:string;

  constructor(private plataformService:PlataformService) { }

  ngOnInit() {
    this.plataformService.getPlataform().subscribe(plataform =>{
      this.currentPlataform = plataform;
    })
  }

}
