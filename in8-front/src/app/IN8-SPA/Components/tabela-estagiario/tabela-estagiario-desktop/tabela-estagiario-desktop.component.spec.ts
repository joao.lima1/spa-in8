import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaEstagiarioDesktopComponent } from './tabela-estagiario-desktop.component';

describe('TabelaEstagiarioComponent', () => {
  let component: TabelaEstagiarioDesktopComponent;
  let fixture: ComponentFixture<TabelaEstagiarioDesktopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaEstagiarioDesktopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaEstagiarioDesktopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
