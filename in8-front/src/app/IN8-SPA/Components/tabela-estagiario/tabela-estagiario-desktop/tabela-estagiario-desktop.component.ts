import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Components, Estagiario } from '../../../Models';
import { EstagiarioService, ScrollService } from '../../../Services';
import { mascaraCelular, mascaraNascimento, mascaraTelefone } from '../../../utils';

@Component({
  selector: 'app-tabela-estagiario-desktop',
  templateUrl: './tabela-estagiario-desktop.component.html',
  styleUrls: ['./tabela-estagiario-desktop.component.scss']
})
export class TabelaEstagiarioDesktopComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  dataSource: MatTableDataSource<Estagiario>;
  displayedColumns = [
    'index',
    'nome',
    'email',
    'nascimento',
    'telefone'
  ];

  arrayEstagiarios: Estagiario[] = [{ telefone: '' }, { telefone: '' }, { telefone: '' }, { telefone: '' }]

  components: Components = new Components();
  mascaraCelular = mascaraCelular;
  mascaraTelefone = mascaraTelefone;
  mascaraNascimento = mascaraNascimento;

  @ViewChild('mainDiv', { static: false }) mainDiv: ElementRef;

  constructor(
    private scrollService: ScrollService,
    private estagiarioService: EstagiarioService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    });
    this.dataSource = new MatTableDataSource<Estagiario>();
    this.dataSource.data = this.arrayEstagiarios;
    this.estagiarioService.getEstagiarios().subscribe(arrayEstagiarios => {
      if (arrayEstagiarios.length) {
        this.fillArrayEstagiario(arrayEstagiarios);
      }
    })
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.scrollService.setYPosition(this.components.Table, this.mainDiv['nativeElement']['offsetTop'])
    }, 1000);
  }

  scrollTo(targetComponent: string) {
    this.scrollService.scrollTo(targetComponent);
  }

  fillArrayEstagiario(newArrayEstagiario: Estagiario[]) {
    newArrayEstagiario.forEach(newEstagiario => {
      let hasPushed: boolean = false;
      this.arrayEstagiarios.forEach(estagiario => {
        if (!estagiario.nome && !hasPushed) {
          const Index = this.arrayEstagiarios.findIndex(targetEstagiario => targetEstagiario === estagiario)
          this.arrayEstagiarios[Index] = newEstagiario;
          hasPushed = true;
        }
      })
      if (!hasPushed) {
        this.arrayEstagiarios.push(newEstagiario);
      }
    })
    this.dataSource.data = this.arrayEstagiarios;
  }

}
