import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaEstagiarioTabletComponent } from './tabela-estagiario-tablet.component';

describe('TabelaEstagiarioTabletComponent', () => {
  let component: TabelaEstagiarioTabletComponent;
  let fixture: ComponentFixture<TabelaEstagiarioTabletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaEstagiarioTabletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaEstagiarioTabletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
