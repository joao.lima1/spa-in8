import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaEstagiarioMobileComponent } from './tabela-estagiario-mobile.component';

describe('TabelaEstagiarioMobileComponent', () => {
  let component: TabelaEstagiarioMobileComponent;
  let fixture: ComponentFixture<TabelaEstagiarioMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaEstagiarioMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaEstagiarioMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
