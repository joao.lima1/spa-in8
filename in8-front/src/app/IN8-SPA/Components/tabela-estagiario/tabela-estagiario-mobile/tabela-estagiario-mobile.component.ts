import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Components, Estagiario } from '../../../Models';
import { EstagiarioService, ScrollService } from '../../../Services';
import { mascaraCelular, mascaraNascimento, mascaraTelefone } from '../../../utils';

@Component({
  selector: 'app-tabela-estagiario-mobile',
  templateUrl: './tabela-estagiario-mobile.component.html',
  styleUrls: ['./tabela-estagiario-mobile.component.scss']
})
export class TabelaEstagiarioMobileComponent implements OnInit, AfterViewInit {

  arrayEstagiarios: Estagiario[] = []
  activeIndex: number = 0
  arrayMobileTable: any[] = [
    [
      {
        key: 'NOME',
        value: ''
      },
      {
        key: 'E-MAIL',
        value: ''
      },
      {
        key: 'NASC',
        value: ''
      },
      {
        key: 'TEL.',
        value: ''
      }
    ],
    [
      {
        key: 'NOME',
        value: ''
      },
      {
        key: 'E-MAIL',
        value: ''
      },
      {
        key: 'NASC',
        value: ''
      },
      {
        key: 'TEL.',
        value: ''
      }
    ],
    [
      {
        key: 'NOME',
        value: ''
      },
      {
        key: 'E-MAIL',
        value: ''
      },
      {
        key: 'NASC',
        value: ''
      },
      {
        key: 'TEL.',
        value: ''
      }
    ],
    [
      {
        key: 'NOME',
        value: ''
      },
      {
        key: 'E-MAIL',
        value: ''
      },
      {
        key: 'NASC',
        value: ''
      },
      {
        key: 'TEL.',
        value: ''
      }
    ],
  ]
  components: Components = new Components();
  mascaraCelular = mascaraCelular;
  mascaraTelefone = mascaraTelefone;
  mascaraNascimento = mascaraNascimento;

  @ViewChild('mainDiv', { static: false }) mainDiv: ElementRef;

  constructor(
    private scrollService: ScrollService,
    private estagiarioService: EstagiarioService
  ) { }

  ngOnInit() {
    this.estagiarioService.getEstagiarios().subscribe(arrayEstagiarios => {
      if (arrayEstagiarios.length) {
        this.fillArrayEstagiario(arrayEstagiarios);
      }
    })
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.scrollService.setYPosition(this.components.Table, this.mainDiv['nativeElement']['offsetTop'])
    }, 1000);
  }

  scrollTo(targetComponent: string) {
    this.scrollService.scrollTo(targetComponent);
  }

  fillMobileTableArray() {
    this.arrayEstagiarios.forEach(estagiario => {
      let hasPushed: boolean = false;
      this.arrayMobileTable.forEach(estagiarioObj => {
        if (!estagiarioObj[0]['value'].length && !hasPushed) {
          hasPushed = true
          const NewEstagiarioObj = [
            {
              key: 'NOME',
              value: estagiario.nome
            },
            {
              key: 'E-MAIL',
              value: estagiario.email
            },
            {
              key: 'NASC',
              value: estagiario.nascimento
            },
            {
              key: 'TEL.',
              value: estagiario.telefone
            }
          ]
          const Index = this.arrayMobileTable.findIndex(targetEstagiarioObj => targetEstagiarioObj === estagiarioObj)
          this.arrayMobileTable[Index] = NewEstagiarioObj;
        }
      })

      if (!hasPushed) {
        this.arrayMobileTable.push(
          [
            {
              key: 'NOME',
              value: estagiario.nome
            },
            {
              key: 'E-MAIL',
              value: estagiario.email
            },
            {
              key: 'NASC',
              value: estagiario.nascimento
            },
            {
              key: 'TEL.',
              value: estagiario.telefone
            }
          ]
        )
      }
    })
  }

  setActiveIndex(newIndex: number) {
    this.activeIndex = newIndex
  }

  fillArrayEstagiario(newArrayEstagiario: Estagiario[]) {
    newArrayEstagiario.forEach(newEstagiario => {
      let hasPushed: boolean = false;
      this.arrayEstagiarios.forEach(estagiario => {
        if (!estagiario.nome && !hasPushed) {
          const Index = this.arrayEstagiarios.findIndex(targetEstagiario => targetEstagiario === estagiario)
          this.arrayEstagiarios[Index] = newEstagiario;
          hasPushed = true;
        }
      })
      if (!hasPushed) {
        this.arrayEstagiarios.push(newEstagiario);
      }
    })
    this.fillMobileTableArray();
  }

}
