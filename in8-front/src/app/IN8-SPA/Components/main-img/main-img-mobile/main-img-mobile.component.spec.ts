import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainImgMobileComponent } from './main-img-mobile.component';

describe('MainImgMobileComponent', () => {
  let component: MainImgMobileComponent;
  let fixture: ComponentFixture<MainImgMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainImgMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainImgMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
