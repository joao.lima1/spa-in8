import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainImgDesktopComponent } from './main-img-desktop.component';

describe('MainImgComponent', () => {
  let component: MainImgDesktopComponent;
  let fixture: ComponentFixture<MainImgDesktopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainImgDesktopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainImgDesktopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
