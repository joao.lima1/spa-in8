import { Component, OnInit } from '@angular/core';
import { ScrollService } from '../../../Services'
import { Components } from '../../../Models'

@Component({
  selector: 'app-main-img-desktop',
  templateUrl: './main-img-desktop.component.html',
  styleUrls: ['./main-img-desktop.component.scss']
})
export class MainImgDesktopComponent implements OnInit {

  components:Components = new Components();

  constructor(private scrollService:ScrollService) { }

  ngOnInit() {
  }

  scrollTo(targetComponent: string){
    this.scrollService.scrollTo(targetComponent);
  }

}
