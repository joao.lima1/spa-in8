import { Component, OnInit, OnDestroy, ViewChild  } from '@angular/core';
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import { MatMenuTrigger, MatMenu } from '@angular/material/menu';
import { ScrollService } from '../../../Services'
import { Components } from '../../../Models'

@Component({
  selector: 'app-main-img-tablet',
  templateUrl: './main-img-tablet.component.html',
  styleUrls: ['./main-img-tablet.component.scss']
})
export class MainImgTabletComponent implements OnInit, OnDestroy {

  isOpen:boolean = false;

  @ViewChild('menuTrigger', { static: false }) menu: MatMenuTrigger;
  @ViewChild('menu', { static: false }) matMenu: MatMenu;

  components:Components = new Components();

  constructor(private scrollService:ScrollService) { }

  ngOnInit() {
  }

  togleMenuStatus(isOpen){
    this.isOpen = isOpen
    setTimeout(() => {
      if(this.menu && this.isOpen){
        setTimeout(() => {
          this.matMenu.overlapTrigger = true
          this.menu.openMenu();
          disableBodyScroll(this.menu);
        });
      }
    });
    if(!this.isOpen){
      enableBodyScroll(this.menu);
    }
  }

  scrollTo(targetComponent: string){
    this.scrollService.scrollTo(targetComponent);
  }

  ngOnDestroy(){
    clearAllBodyScrollLocks();
  }

}
