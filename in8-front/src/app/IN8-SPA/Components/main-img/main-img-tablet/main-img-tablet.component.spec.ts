import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainImgTabletComponent } from './main-img-tablet.component';

describe('MainImgTabletComponent', () => {
  let component: MainImgTabletComponent;
  let fixture: ComponentFixture<MainImgTabletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainImgTabletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainImgTabletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
