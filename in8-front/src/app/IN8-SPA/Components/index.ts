export * from './alert/alert.component';
export * from './cadastro-estagiario/cadastro-estagiario.component';
export * from './in8-spa/in8-spa.component';
export * from './main-footer';
export * from './main-img';
export * from './progress-spinner-dialog/progress-spinner-dialog.component';
export * from './tabela-estagiario';
