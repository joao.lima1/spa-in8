import {
  inject,
  lifeCycleObserver,
  LifeCycleObserver,
  ValueOrPromise
} from '@loopback/core';
import {juggler} from '@loopback/repository';
const config = require('./in8.datasource.config');

@lifeCycleObserver('datasource')
export class In8DataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'in8';

  constructor(
    @inject('datasources.config.in8', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }

  /**
   * Start the datasource when application is started
   */
  start(): ValueOrPromise<void> {
    // Add your logic here to be invoked when the application is started
  }

  /**
   * Disconnect the datasource when application is stopped. This allows the
   * application to be shut down gracefully.
   */
  stop(): ValueOrPromise<void> {
    return super.disconnect();
  }
}
