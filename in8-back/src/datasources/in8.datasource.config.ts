module.exports = {
  connector: "loopback-connector-firestore",
  name: "in8",
  projectId: process.env.DB_PROJECT_ID,
  clientEmail: process.env.DB_CLIENT_EMAIL,
  privateKey: process.env.DB_PRIVATE_KEY,
  databaseName: "Optional, Default: projectId",
}
