import {DefaultCrudRepository} from '@loopback/repository';
import {Estagiario, EstagiarioRelations} from '../models';
import {In8DataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EstagiarioRepository extends DefaultCrudRepository<
  Estagiario,
  typeof Estagiario.prototype.id,
  EstagiarioRelations
> {
  constructor(
    @inject('datasources.in8') dataSource: In8DataSource,
  ) {
    super(Estagiario, dataSource);
  }
}
