import {ApplicationConfig} from '@loopback/core';
import {SpaIn8Application} from './application';

export {SpaIn8Application};

export async function main(options: ApplicationConfig = {}) {
  const app = new SpaIn8Application(options);
  await app.boot();
  await app.migrateSchema();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);

  return app;
}
