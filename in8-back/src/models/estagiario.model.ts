import {Entity, model, property} from '@loopback/repository';

@model()
export class Estagiario extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nome: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  nascimento: string;

  @property({
    type: 'string',
    required: true,
  })
  telefone: string;


  constructor(data?: Partial<Estagiario>) {
    super(data);
  }
}

export interface EstagiarioRelations {
  // describe navigational properties here
}

export type EstagiarioWithRelations = Estagiario & EstagiarioRelations;
