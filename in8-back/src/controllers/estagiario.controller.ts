import {
  repository
} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,

  HttpErrors, post,



  requestBody
} from '@loopback/rest';
import { Estagiario } from '../models';
import { EstagiarioRepository } from '../repositories';

export class EstagiarioController {
  constructor(
    @repository(EstagiarioRepository)
    public estagiarioRepository: EstagiarioRepository,
  ) { }

  @post('/estagiarios', {
    responses: {
      '200': {
        description: 'Estagiario model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Estagiario) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Estagiario, {
            title: 'NewEstagiario',
            exclude: ['id'],
          }),
        },
      },
    })
    estagiario: Omit<Estagiario, 'id'>,
  ): Promise<Estagiario> {
    try {
      const Response = await this.estagiarioRepository.create(estagiario);
      delete Response.id
      return Response
    }
    catch (Erro) {
      throw new HttpErrors.BadGateway(Erro.message)
    }
  }

  @get('/estagiarios', {
    responses: {
      '200': {
        description: 'Array of Estagiario model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Estagiario, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async getAll(): Promise<Estagiario[]> {
    try {
      const Response = await this.estagiarioRepository.find();
      if (Response.length) {
        for (const Estagiario of Response) {
          delete Estagiario.id
        }
      }
      return Response
    }
    catch (Erro) {
      throw new HttpErrors.BadGateway(Erro.message)
    }
  }

}
