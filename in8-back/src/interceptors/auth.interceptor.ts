import {
  globalInterceptor,
  Interceptor,
  InvocationContext,
  InvocationResult,
  Provider,
  ValueOrPromise
} from '@loopback/context';
import { HttpErrors, RestBindings } from '@loopback/rest';


/**
 * This class will be bound to the application as an `Interceptor` during
 * `boot`
 */
@globalInterceptor('Auth', { tags: { name: 'Auth' } })
export class AuthInterceptor implements Provider<Interceptor> {
  /*
  constructor() {}
  */

  /**
   * This method is used by LoopBack context to produce an interceptor function
   * for the binding.
   *
   * @returns An interceptor function
   */
  value() {
    return this.intercept.bind(this);
  }

  /**
   * The logic to intercept an invocation
   * @param invocationCtx - Invocation context
   * @param next - A function to invoke next interceptor or the target method
   */
  async intercept(
    invocationCtx: InvocationContext,
    next: () => ValueOrPromise<InvocationResult>,
  ) {
    try {
      const httpReq = await invocationCtx.get(RestBindings.Http.REQUEST, { optional: true, });
      let authToken
      if (httpReq) {
        if (httpReq.headers) {
          authToken = httpReq.headers.authtoken;
        }
        if (authToken === process.env.AUTH_TOKEN) {
          const result = await next();
          return result;
        }
        else {
          throw new HttpErrors.Unauthorized('Token Invalido');
        }
      }
    } catch (err) {
      throw err;
    }
  }
}
