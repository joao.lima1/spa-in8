# Prova Estagio IN8 João Victor Cardoso de Lima

## Ferramentas utilizadas

- Angular 8 para o Front-End
- NodeJs junto com o framework Loopback 4 para o Back-End
- Firebase Cloud Firestore para o BD
- gCloud para hospedar o BackEnd
- Firebase Hosting para hospedar o FrontEnd

## Build

### Front

- Execute `npm install` dentro do diretorio `in8-front`
- Execute `npm start`
- A aplicação estara online no localhost:4200

### Back

- Execute `npm install` dentro do diretorio `in8-back`
- Execute `npm start`
- A aplicação estara online no localhost:3000

